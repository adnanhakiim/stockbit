FROM nginx:latest
COPY ./hello.txt /var/www/hello.txt
CMD ["nginx", "-g", "daemon off;"]
